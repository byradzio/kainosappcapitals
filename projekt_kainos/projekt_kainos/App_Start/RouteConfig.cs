﻿using System.Web.Mvc;
using System.Web.Routing;

namespace projekt_kainos
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                "Compare",
                "Compare",
                new {controller = "Capitals", action = "Compare"}
                );

            routes.MapRoute("Default", "{controller}/{action}/{id}",
                new {controller = "Capitals", action = "Index", id = UrlParameter.Optional}
                );
        }
    }
}